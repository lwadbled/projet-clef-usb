import os
import mimetypes
import io
import pandas as pd
from datetime import datetime

from Google import Create_Service
from Google import MediaFileUpload
from googleapiclient.http import MediaIoBaseDownload

#Création du service Google Drive API

CREATE_SECRET_FILE = r"C:\\Users\Zemmouri Thomas\Documents\Polytech\4A\usb\code_secret_client_800995775336-9sagt060t33nal0bj10qsrca8cslpe8u.apps.googleusercontent.com.json"
API_NAME ='drive'
API_VERSION ='v3'
SCOPES = ['https://www.googleapis.com/auth/drive']

service = Create_Service(CREATE_SECRET_FILE,API_NAME,API_VERSION,SCOPES)#création du service, comprend l'affichage d'un message un fois abouti


#Définition des fonctions pour le parcours récursif des dossiers

#fonction de traitement de la création et du parcours d'un sous dossier
def downloaddossier(iddossier,nom,dossierarrivee):
    query= f"parents = '{iddossier}'"
    os.mkdir(dossierarrivee+r'\\'+nom) #création du dossier sue le stockage physique, au même nom que le dossier sur le drive
    response=service.files().list(q=query).execute()
    files=response.get('files')#liste contenant les différents éléments (fichier ou dossier)
    nextPageToken=response.get('nextPageToken')
    while nextPageToken: #permet d'assurer que tout les éléments ont été pris en compte
        response=service.files().list(q=query).execute()
        files.extend(response.get('files'))
        nextPageToken=response.get('nextPageToken')
    for elem in files: #parcours du dossier/sous dossier
        elemdown=elem
        if (elemdown['mimeType']=='application/vnd.google-apps.folder'):# la fonction s'appelle de manière récursive si l'élément est un sous dossier
            downloaddossier(elemdown['id'],elemdown['name'],dossierarrivee+r'\\'+nom)
        else :#la fonction appelle la création du fichier sur le drive si c'est un fichier
            print(elemdown['name'])
            downloadfichier(elemdown['id'],elemdown['name'],dossierarrivee+r'\\'+nom)
   

    
#fonction de traitement de l'upload des fichiers rencontrés
def downloadfichier(idfich,nom,dossierarrivee):    #permet le download d'un fichier depuis le drive
    request=service.files().get_media(fileId=idfich)
    fh= io.BytesIO()
    downloader = MediaIoBaseDownload(fd=fh, request=request)#création du média de download
    done=False
    while not done:#attente du téléchargement total du fichier
        status, done = downloader.next_chunk()
        print('Download progress (0)', format(status.progress() *100))
    fh.seek(0)
    filepath=dossierarrivee
    with open(os.path.join(filepath,nom), 'wb') as f: #'copie' du fichier téléchargé, écrit au niveau du dossier parent
        f.write(fh.read())
        f.close()


#Application du Download
page_token = None
dossierssuggeres=[]#liste des dossiers qui seront suggérés à l'utilisateur la chaine de caractère qu'il entre est contenu dans le nom d'un dossier existant
dossierstrouves=[]
while (len(dossierstrouves)!=1): #on demande à l'utilisateur un nom de dossier tant que sa selection ne correspond pas à un dossier unique
    Dossier_a_Up=input('Renseignez un dossier à chercher sur votre Drive:')
    queryname="name = '"+Dossier_a_Up+"'" #la chaine "name =..." signifie que l'ont cherche le nom du dossier au caractère prêt
    while True:
        response = service.files().list(q=queryname,
                                          spaces='drive',
                                          fields='nextPageToken, files(id, name)',
                                          pageToken=page_token).execute()
        dossierstrouves=response.get('files',[]) #les élements correspondant sont listés et enregistrés avant des parcourus et affichés
        if (len(dossierstrouves)!=0):
                for fichier in dossierstrouves:
                    print('Found file: %s (%s)' % (fichier.get('name'), fichier.get('id')))
                    page_token = response.get('nextPageToken', None)
                if page_token is None:
                    break
        else :
            print('Aucun dossier trouvé avec le nom',Dossier_a_Up) #si aucun dossier n'est trouvé avec le nom exacte renseigné, on relance la recherche pour suggérer un nom de dossier
            queryname="name contains '"+Dossier_a_Up+"' and mimeType= 'application/vnd.google-apps.folder'" #"name contains" indique que la recherche concerne les dossiers contenant la chaine de caractère
            while True:
                response = service.files().list(q=queryname,
                                          spaces='drive',
                                          fields='nextPageToken, files(id, name)',
                                          pageToken=page_token).execute()
                dossierssuggeres=response.get('files',[])
                if (len(dossierssuggeres)!=0):#une fois des dossiers trouvés et listés, on suggère à l'utilisateur de regarder la liste avant de proposer un nouveau nom
                    print('Vouliez vous dire: \n')
                    for fichier in dossierssuggeres:
                        print('Dossier trouvé: %s (%s)' % (fichier.get('name'), fichier.get('id')))
                        page_token = response.get('nextPageToken', None)
                    if page_token is None:
                        break
                break
            break

#lorsque l'on sort de la boucle while, on est sure qu'il n'y a qu'un seul fichier possible à télécharger
print('Le dossier à Uploader est:',dossierstrouves[0])


#Téléchargement du dossier

#on demande à l'utilisateur de paramétrer son téléchargement
PathDossierUSB=input("Renseingez l'adresse a laquelle download le dossier: (par defaut C:\)  ")#on demande le dossier d'arrivée
if (PathDossierUSB==''):
    PathDossierUSB(r'C:\\')
NomdossierUSB=input("\n Comment voulez vous appeler votre dossier sur votre ordinateur? (par defaut 'DossierBackUp+date actuelle)  ")#le nom d'arrivée
if (NomdossierUSB==''):
    now = datetime.now()
    dt_string = now.strftime("_%d_%m_%Y_%H-%M-%S")
    print("date and time =", dt_string)
    NomdossierUSB='DossierBackUp_'+dt_string #on donne un nom par défaut contenant la date et lheure au dossier 

# on peut lancer le parcours/téléchargement récursif du dossier choisi grâce à la fonction downloaddossier
downloaddossier(dossierstrouves[0].get('id'),NomdossierUSB,PathDossierUSB)

print('Téléchargement terminé\n')

