import os
import mimetypes
from tkinter import *
from datetime import datetime
from Google import Create_Service
from Google import MediaFileUpload

def saisieNom():
    Nomdossier=champNom.get()
    lbl4=Label(window1,text='Saisi')
    lbl4.place(x=560,y=40)
    champNom.configure(state='disabled')

def saisieChemin():
    Dossier_a_Up=champPath.get()
    lbl5=Label(window1,text='Saisi')
    lbl5.place(x=560,y=10)
    champPath.configure(state='disabled')

def clicked():


    print("hello world! \n")
    id_dossier_drive = '1b0t6hKEAfNJWDM5yiIUVdqeEWDSUpaRU'
    CREATE_SECRET_FILE = r"C:\\Users\Zemmouri Thomas\Documents\Polytech\4A\usb\code_secret_client_800995775336-9sagt060t33nal0bj10qsrca8cslpe8u.apps.googleusercontent.com.json"
    API_NAME ='drive'
    API_VERSION ='v3'
    SCOPES = ['https://www.googleapis.com/auth/drive']

    service = Create_Service(CREATE_SECRET_FILE,API_NAME,API_VERSION,SCOPES)

    n=85
    p=20
    NomdossierDrive=str(champNom.get())
    if (NomdossierDrive==''):
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        text2=Label(window1)
        text2.configure(text="date and time ="+ dt_string,anchor='e',justify=LEFT)
        text2.place(x=5,y=n)
        n+=p
        NomdossierDrive='DossierBackUp :'+dt_string

    metadonnee = {'name':NomdossierDrive,'mimeType':'application/vnd.google-apps.folder','parents':[]}
    dossierdrive=service.files().create(body=metadonnee).execute()
    text3=Label(window1)
    text3.configure(text='ID du dossier drive:'+dossierdrive.get('id') ,anchor='e',justify=LEFT)
    text3.place(x=5,y=n)
    n+=p


    idsdir=[]
    idsdir.append(dossierdrive.get('id'))
    i=1
    pathlen=0

    Dossier_a_Up=champPath.get()
    text4=Label(window1)
    text4.configure(text='Dossier à Uploader:'+Dossier_a_Up,anchor='e',justify=LEFT)
    text4.place(x=5,y=n)
    n+=p 
    print('Dossier à Uploader:', Dossier_a_Up)
    for window, dirs, files in os.walk(Dossier_a_Up):

        path = window.split(os.sep)
        #print(len(path))
        if (len(path)<=pathlen):
            idsdir.pop()
        metadonnee = {'name':os.path.basename(window),'mimeType':'application/vnd.google-apps.folder','parents':[idsdir[len(idsdir)-1]]}
        dosid=service.files().create(body=metadonnee).execute()
        idsdir.append(dosid.get('id'))
        #print ('ID du dossier drive: %s\n' % dossierdrive.get('id'))
        Dossier_a_Up=champPath.get()
        text5=Label(window1)
        text5.configure(text=(len(path)+2) * '---'+'dossier:'+os.path.basename(window),anchor='e',justify=LEFT)
        text5.place(x=5,y=n)
        n+=p
        print((len(path)+2) * '---','dossier:', os.path.basename(window))
        for file in files:
            fichier_mimeType=mimetypes.guess_type(file, strict=True)
            #print('type du fichier',fichier_mimeType)
            dossierpath=window
            fichpath=dossierpath+r'\\'+file
            fich_meta= {'name':file, 'parents':[idsdir[len(idsdir)-1]]}
            media = MediaFileUpload(fichpath,mimetype=fichier_mimeType[0], resumable=True)
            file2= service.files().create(body=fich_meta, media_body=media, fields='id').execute()
            text6=Label(window1)
            text6.configure(text=len(path) * '---'+'nom du fichier: '+file+'type du fichier:'+str(fichier_mimeType)+ "adrsse d'origine"+window,anchor='e',justify=LEFT)
            text6.place(x=5,y=n)
            n+=p
            print(len(path) * '---','nom du fichier: ', file, 'type du fichier:', fichier_mimeType, "adrsse d'origine", window)
        pathlen=len(path)

    text7=Label(window1)
    text7.configure(text="Fin de l'upload ",anchor='e',justify=LEFT)
    text7.place(x=5,y=n)
    n+=p 
    print("\n Fin de l'upload \n")
        

        

    
    print('\n\n\n')  

window1=Tk()
window1.geometry('1000x600')
window1.title('Upload vers Google Drive')
fen=Canvas(window1, height=400, width=300) 

lbl1=Label(window1, text="Comment voulez vous appeler votre dossier sur le Drive?\n (par defaut 'DossierBackUp +date actuelle)")
lbl2=Label(window1, text="Renseigner l'adresse du dossier à Uploader : ")

text2=Label(window1)
textesaisi=''

textNom=Label(window1, text="Nom du dossier sur le drive \n(par defaut 'DossierBackUp +date actuelle)",anchor='e', justify=LEFT)
textPath=Label(window1, text="Chemin du dossier local à uploader \n",anchor='e', justify=LEFT)
textNom.place(x=5, y=40)
textPath.place(x=5, y=10)

btn1 = Button(window1, text="Let's Upload", command=clicked)
btn2= Button(window1, text="Saisir", command=saisieChemin)
btn3 = Button(window1, text="Saisir", command=saisieNom)
btn3.place(x=520, y=40)
btn2.place(x=520, y=10)
btn1.place(x=5, y=80)

champNom = Entry(window1,width=40)
champPath= Entry(window1,width=40)
champNom.place(x=275, y=40)
champPath.place(x=275, y=10)

window1.mainloop()