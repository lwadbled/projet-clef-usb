import os
import mimetypes
from datetime import datetime
from Google import Create_Service
from Google import MediaFileUpload


#Création du service Google Drive API
id_dossier_drive = '1b0t6hKEAfNJWDM5yiIUVdqeEWDSUpaRU'
#l'emplacement du fichier code secret client doit nécessairement est connu et spécifié dans le code
CREATE_SECRET_FILE = r"C:\\Users\Zemmouri Thomas\Documents\Polytech\4A\usb\code_secret_client_800995775336-9sagt060t33nal0bj10qsrca8cslpe8u.apps.googleusercontent.com.json"

API_NAME ='drive'
API_VERSION ='v3'
SCOPES = ['https://www.googleapis.com/auth/drive']

service = Create_Service(CREATE_SECRET_FILE,API_NAME,API_VERSION,SCOPES)


#Application de l'Uplaod
#on demande à l'utilisateur de paramétrer son dossier initiale qui contiendra le contenu voulu
NomdossierDrive=input("Comment voulez vous appeler votre dossier sur le Drive? (par defaut 'DossierBackUp+date actuelle)  ")
if (NomdossierDrive==''):
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("date and time =", dt_string)
    NomdossierDrive='DossierBackUp :'+dt_string#on attribue un nom par défaut contenant date+heure si l'utilisateur n'en saisit pas

metadonnee = {'name':NomdossierDrive,'mimeType':'application/vnd.google-apps.folder','parents':[]}#on paramètre ensuite les métadonnée du dossier qui sera uploadé à la racine du drive
dossierdrive=service.files().create(body=metadonnee).execute()#création du dossier dans le drive
print('ID du dossier drive: %s\n' % dossierdrive.get('id'))  


#upload du contenu du dosier dans le dossier précédemment crée dans le drive

idsdir=[]#liste des identifiants des repertoires succéssifs que l'on va trouver
idsdir.append(dossierdrive.get('id'))#on l'initialise avec l'id du dossier initiale
#Cette liste sera soit remplie ("liste.append") soit raccourci ("liste.pop") au fur et mesure, en fonction de l'avancée du parcours récursif.
#lors qe l'algorymthe "descend" dans l'arborescence on rajoute l'id du dossier, lorsqu'il "remonte" on pop la dernière valeur
pathlen=0

Dossier_a_Up=input("Renseigner l'adresse du dossier à Uploader : \n")#l'utlisateur paramètre son upload
print('Dossier à Uploader:', Dossier_a_Up)
for chemin, dirs, files in os.walk(Dossier_a_Up):#on parcours les dossiers et les fichiers, chaque "noeud" de l'arbre est caractérisé par son path, sa liste de dossier et de fichiers
    path = chemin.split(os.sep)
    if (len(path)<=pathlen):#si le path (chemin d'accès) actuel est plus court ou égale à celui d'avant, cela signifie que l'on n'est pas rentré dans un sous dossier
        idsdir.pop()
    
    #on crée le dossier
    metadonnee = {'name':os.path.basename(chemin),'mimeType':'application/vnd.google-apps.folder','parents':[idsdir[len(idsdir)-1]]}
    dossiercree=service.files().create(body=metadonnee).execute()
    idsdir.append(dossiercree.get('id'))
    print((len(path)+2) * '---','dossier:', os.path.basename(chemin))


    #upload des fichiers

    for file in files: #on parcours les fichiers
        fichier_mimeType=mimetypes.guess_type(file, strict=True) #on utilise mimetypes.guess_type pour déduire le type du fichier en fonction de son nom
        dossierpath=chemin
        fichpath=dossierpath+r'\\'+file #on renseingne les métadonnées pour upload le fichier à l'endroit où en est le parcours recursif
        fich_meta= {'name':file, 'parents':[idsdir[len(idsdir)-1]]} #on utilise le dernier élément de la liste des id's pour savoir où upload
        media = MediaFileUpload(fichpath,mimetype=fichier_mimeType[0], resumable=True)
        #on execute l'upload grâce au service gdrive api
        file2= service.files().create(body=fich_meta, media_body=media, fields='id').execute()
        print(len(path) * '---','nom du fichier: ', file, 'type du fichier:', fichier_mimeType, "adresse d'origine", path)
    pathlen=len(path)#on enregistre la longueur du chemin d'accès pour pouvoir le comparé au prochain

print("\n Fin de l'upload \n")
    
 
print('\n\n\n') 

