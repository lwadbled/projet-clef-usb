/** Main file for USB storage key **/

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>

#include "io.h"
#include "spi.h"
#include "AT45DB641E.h"

// Constants

#define MAX_LED		1

#define LED		0

#define PORTB_OUTPUT	0x40
#define PORTC_OUTPUT	0x00
#define PORTD_OUTPUT	0x00

unsigned char led1[]={0x40,0x00,0x00};

unsigned char *leds[]={led1};

#define MEM_RESET_DDR	DDRB
#define MEM_RESET_PORT	PORTB
#define MEM_RESET_PIN	5

#define MEM1_DDR	DDRD
#define MEM1_PORT	PORTD
#define MEM1_PIN	0

//#define MEM2_DDR	DDRB
//#define MEM2_PORT	PORTB
//#define MEM2_PIN	4

void blink_byte(uint8_t byte,int led){
int i;
for(i=0;i<8;i++){
  int nb=(byte&0x01)?2:1;
  int j;
  for(j=0;j<nb;j++){
    output_set(leds,led); _delay_ms(100);
    output_unset(leds,led); _delay_ms(100);
    }
  _delay_ms(1000);
  byte >>= 1;
  }
}

int main(void)
{
unsigned char omasks[]={PORTB_OUTPUT,PORTC_OUTPUT,PORTD_OUTPUT};
outputs_init(omasks);
int i;
for(i=0;i<10;i++){
  output_set(leds,LED); _delay_ms(50);
  output_unset(leds,LED); _delay_ms(1000);
  }
_delay_ms(1000);

MEM1_DDR |= (1<<MEM1_PIN);
//MEM2_DDR |= (1<<MEM2_PIN);
MEM1_PORT |= (1<<MEM1_PIN);
//MEM2_PORT |= (1<<MEM2_PIN);
MEM_RESET_DDR |= (1<<MEM_RESET_PIN);
MEM_RESET_PORT |= (1<<MEM_RESET_PIN);
spi_init();

unsigned char data[]={0x01,0x02,0x03,0x04,0xff};
unsigned char copy[]={0x00,0x00,0x00,0x00,0x00};
int len=sizeof(data);
AT45DB641E_page_size(&MEM1_PORT,MEM1_PIN,256);
//AT45DB641E_page_size(&MEM2_PORT,MEM2_PIN,256);
AT45DB641E_write_buffer(&MEM1_PORT,MEM1_PIN,data,len,SEQ_START|SEQ_STOP);
AT45DB641E_write_page(&MEM1_PORT,MEM1_PIN,0);
AT45DB641E_read_page(&MEM1_PORT,MEM1_PIN,0,copy,len,SEQ_START|SEQ_STOP);
for(i=0;i<sizeof(copy);i++) copy[i] *= 2;
/*AT45DB641E_write_buffer(&MEM1_PORT,MEM1_PIN,copy,len,SEQ_START|SEQ_STOP);
AT45DB641E_write_page(&MEM1_PORT,MEM1_PIN,0);
AT45DB641E_read_page(&MEM1_PORT,MEM1_PIN,0,data,len,SEQ_START|SEQ_STOP);
*/
for(i=0;i<sizeof(data);i++) blink_byte(copy[i],LED);

return 0;
}
