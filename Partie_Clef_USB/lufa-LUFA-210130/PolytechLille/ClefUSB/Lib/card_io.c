/** Handle pad I/O **/

#include "io.h"
#include "card_io.h"

// Global variables

uint32_t leds[MAX_LED]={
  0x00400000,
  0x00000080, 0x00000040, 0x00000020, 0x00000002,
  0x00000400, 0x00000008, 0x00000004, 0x00000010
  };
uint32_t displays[MAX_DISPLAY]={
  0x00001000, 0x00002000, 0x00800000
  };
uint32_t outstate=0x00000000;
uint32_t buttons[MAX_BUTTON]={};

