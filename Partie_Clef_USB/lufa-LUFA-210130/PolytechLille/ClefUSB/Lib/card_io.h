/** Definitions for pad I/O **/

#include <stdint.h>

// Constants

#define MAX_LED		(8+1)
#define MAX_DISPLAY	3
#define MAX_BUTTON	0

#define OUTPUT_PORTB	0x80
#define OUTPUT_PORTC	0x74
#define OUTPUT_PORTD	0xFE

#define INPUT_PORTB	0x00
#define INPUT_PORTC	0x00
#define INPUT_PORTD	0x00

// Macros

#define LED_set(mask)		\
  { outstate |= mask; outputs_set((outstate&0x00ff0000)>>16,(outstate&0x0000ff00)>>8,(outstate&0x000000ff)); }
#define LED_unset(mask)		\
  { outstate &= ~mask; outputs_set((outstate&0x00ff0000)>>16,(outstate&0x0000ff00)>>8,(outstate&0x000000ff)); }
#define LED_toggle(mask)	\
  { outstate ^= mask; outputs_set((outstate&0x00ff0000)>>16,(outstate&0x0000ff00)>>8,(outstate&0x000000ff)); }

#define DISP_set(mask)		LED_set(mask)
#define DISP_unset(mask)	LED_unset(mask)

// Global variables

extern uint32_t leds[MAX_LED];
extern uint32_t displays[MAX_DISPLAY];
extern uint32_t outstate;
extern uint32_t buttons[MAX_BUTTON];
