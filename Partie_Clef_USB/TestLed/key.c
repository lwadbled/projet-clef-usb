/** Main file for USB storage key **/

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#include "io.h"

// Constants

#define MAX_LED	1

#define PORTB_OUTPUT	0x40
#define PORTC_OUTPUT	0x00
#define PORTD_OUTPUT	0x00

unsigned char led1[]={0x40,0x00,0x00};

unsigned char *leds[]={led1};

int main(void)
{
unsigned char omasks[]={PORTB_OUTPUT,PORTC_OUTPUT,PORTD_OUTPUT};
outputs_init(omasks);
int i;
while(1){
  for(i=0;i<MAX_LED;i++){ output_set(leds,i); _delay_ms(1000); }
  for(i=0;i<MAX_LED;i++){ output_unset(leds,i); _delay_ms(1000); }
  }
return 0;
}
