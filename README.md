# Projet Clef USB

Projet SE Clef USB avec stockage distant (à l'aide de l'API Google Drive) de Coline Lemoine, Thomas Zemmouri et Louis Wadbled.

## Composition

Le dépot est composé de deux répertoires, l'un pour les codes de tests et de programmation de la clef USB, l'autre pour les codes de téléchargement des fichiers entre la clef et Google Drive.

## Partie Clef USB

Les codes de tests (se trouvant dans les répertoires TestLed,TestMem et TestMemRW) ont été écrits par Mr.Redon. La partie programmation de la clef à l'aide la bibliothèque LUFA se trouve dans le répertoire lufa-LUFA-210130/PolytechLille/ClefUSB.

## Partie API Google Drive

Cette partie est composée de deux fichiers, l'un permettant de télécharger des fichiers et/ou dossiers depuis Google Drive sur la clef USB (download.py) et l'autre permettant de télécharger des fichiers et/ou dossiers vers Google Drive depuis un répertoire choisi (upload.py).

## Contributeurs

Ce projet a été réalisé par Coline LEMOINE, Thomas ZEMMOURI et Louis WADBLED.
